/*= sélectionner la lignes où l'attribut est égal à "1"*/
SELECT * FROM `t_personnes` WHERE `id_Personne` = 1 

/*> sélectionner les ligness où l'attribut est supérieur à "2"*/
SELECT * FROM `t_personnes` WHERE `id_Personne` > 2

/*>= sélectionner les ligness où l'attribut est supérieur ou égal à "2"*/
SELECT * FROM `t_personnes` WHERE `id_Personne` >= 2  

/*< sélectionner les ligness où l'attribut est inférieur à "2"*/
SELECT * FROM `t_personnes` WHERE `id_Personne` < 5 

/*<= sélectionner les ligness où l'attribut est inférieur ou égal à "10"*/
SELECT * FROM `t_personnes` WHERE `id_Personne` <= 10 

/*!= sélectionner les ligness où l'attribut est différent à "10"*/
SELECT * FROM `t_personnes` WHERE `id_Personne` != 10 

/*LIKE sélectionner les lignes où l'attribut correspond à '5'*/
SELECT * FROM `t_personnes` WHERE `id_Personne` LIKE '5' 

/*LIKE %...% sélectionner les lignes où l'attribut qui possede 'org' à n'importe quelle position char*/
SELECT * FROM `t_personnes` WHERE `NomPers` LIKE '%org%'

/*NOT LIKE sélectionner les lignes où l'attribut ne possede pas 'Tripp'*/
SELECT * FROM `t_personnes` WHERE `NomPers` NOT LIKE 'Tripp' 

/*REGEXP sélectionner les lignes où la Regex [A-Za-z0-9] est vrai*/
SELECT * FROM `t_personnes` WHERE `NomPers` REGEXP '[A-Za-z0-9]' 

/*REGEXP^...$ sélectionner les lignes où la Regex [^morgun$] est vrai*/
SELECT * FROM `t_personnes` WHERE `NomPers` REGEXP '^morgun$' 

/*NOT REGEXP sélectionner les lignes où la REGEXP [A-z] est active*/
SELECT * FROM `t_personnes` WHERE `NomPers` NOT REGEXP '[a]' 

/*=" sélectionner les lignes où le champ est vide*/
SELECT * FROM `t_localisations` WHERE `NomLieuLocalisation` = '' 

/*!=" sélectionner les lignes où le champ est non-vide*/
SELECT * FROM `t_personnes` WHERE `NomPers` != '' 

/*NOT IN sélectionner les lignes où la valeur est pas égale à 'Selle'*/
SELECT * FROM `t_personnes` WHERE `NomPers` NOT IN ('Selle') 

/*IN sélectionner les lignes où la valeur est égale à 'Selle'*/
SELECT * FROM `t_personnes` WHERE `NomPers` IN ('Selle')

/*BETWEEN sélectionner les lignes où la valeur se situe entre "40 AND 47"*/
SELECT * FROM `t_localisations` WHERE `GpsLatLocalisation` BETWEEN 40 AND 47 

/*NOT BETWEEN sélectionner les lignes où la valeur ne se situe pas entre "40 AND 47"*/
SELECT * FROM `t_localisations` WHERE `id_Localisation` NOT BETWEEN 1 AND 2

/*NULL sélectionner les lignes où la valeur est nulle*/
SELECT * FROM `t_localisations` WHERE `GpsLatLocalisation` IS NULL 

/*NOT NULL sélectionner les lignes où la valeur n'est pas nulle*/
SELECT * FROM `t_localisations` WHERE `GpsLatLocalisation` IS NOT NULL  