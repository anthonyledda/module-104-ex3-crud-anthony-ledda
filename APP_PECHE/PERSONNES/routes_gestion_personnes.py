# routes_gestion_personnes.py
# OM 2020.04.06 personnes des "routes" FLASK pour les personnes.

from flask import render_template, flash, redirect, url_for, request
from APP_PECHE import obj_mon_application
from APP_PECHE.PERSONNES.data_gestion_personnes import GestionPersonnes
from APP_PECHE.DATABASE.erreurs import *
# OM 2020.04.10 Pour utiliser les expressions régulières REGEX
import re


# ---------------------------------------------------------------------------------------------------
# OM 2020.04.07 Définition d'une "route" /personnes_afficher
# cela va permettre de programmer les actions avant d'interagir
# avec le navigateur par la méthode "render_template"
# Pour tester http://127.0.0.1:1234/personnes_afficher
# ---------------------------------------------------------------------------------------------------
@obj_mon_application.route("/personnes_afficher/<string:order_by>/<int:id_personne_sel>", methods=['GET', 'POST'])
def personnes_afficher(order_by,id_personne_sel):
    # OM 2020.04.09 Pour savoir si les données d'un formulaire sont un affichage
    # ou un envoi de donnée par des champs du formulaire HTML.
    if request.method == "GET":
        try:
            # OM 2020.04.09 Objet contenant toutes les méthodes pour gérer (CRUD) les données.
            obj_actions_personnes = GestionPersonnes()
            # Récupére les données grâce à une requête MySql définie dans la classe Gestionpersonnes()
            # Fichier data_gestion_personnes.py
            data_personnes = obj_actions_personnes.personnes_afficher_data(order_by,id_personne_sel)
            # DEBUG bon marché : Pour afficher un message dans la console.
            print(" data personnes", data_personnes, "type ", type(data_personnes))
            # Différencier les messages si la table est vide.
            if not data_personnes and id_personne_sel == 0:
                flash("""La table "t_personnes" est vide. !!""", "warning")
            elif not data_personnes and id_personne_sel > 0:
                # Si l'utilisateur change l'id_mail dans l'URL et que la personne n'existe pas,
                flash(f"Le mail demandé n'existe pas !!", "warning")
            else:
                # Dans tous les autres cas, c'est que la table "t_mails" est vide.
                # OM 2020.04.09 La ligne ci-dessous permet de donner un sentiment rassurant aux utilisateurs.
                flash(f"Données mails affichés !!", "success")

        except Exception as erreur:
            print(f"RGG Erreur générale.")
            # OM 2020.04.09 On dérive "Exception" par le "@obj_mon_application.errorhandler(404)" fichier "run_mon_app.py"
            # Ainsi on peut avoir un message d'erreur personnalisé.
            # flash(f"RGG Exception {erreur}")
            raise Exception(f"RGG Erreur générale. {erreur}")
            # raise MaBdErreurOperation(f"RGG Exception {msg_erreurs['ErreurNomBD']['message']} {erreur}")

    # OM 2020.04.07 Envoie la page "HTML" au serveur.
    return render_template("personnes/personnes_afficher.html", data=data_personnes)


# ---------------------------------------------------------------------------------------------------
# OM 2020.04.07 Définition d'une "route" /personnes_add ,
# cela va permettre de programmer quelles actions sont réalisées avant de l'envoyer
# au navigateur par la méthode "render_template"
# En cas d'erreur on affiche à nouveau la page "personnes_add.html"
# Pour la tester http://127.0.0.1:1234/personnes_add
# ---------------------------------------------------------------------------------------------------
@obj_mon_application.route("/personnes_add", methods=['GET', 'POST'])
def personnes_add():
    # OM 2019.03.25 Pour savoir si les données d'un formulaire sont un affichage
    # ou un envoi de donnée par des champs utilisateurs.
    if request.method == "POST":
        try:
            # OM 2020.04.09 Objet contenant toutes les méthodes pour gérer (CRUD) les données.
            obj_actions_personnes = GestionPersonnes()
            # OM 2020.04.09 Récupère le contenu du champ dans le formulaire HTML "personne_add.html"
            name_personne = request.form['name_personne_html']
            firstname_personne = request.form['firstname_personne_html']
            datenaissance_personne = request.form['datenaissance_personne_html']
            # OM 2019.04.04 On ne doit pas accepter des valeurs vides, des valeurs avec des chiffres,
            # des valeurs avec des caractères qui ne sont pas des lettres.
            # Accepte le trait d'union ou l'apostrophe, et l'espace entre deux mots, mais pas plus d'une occurence.
            if not re.match("^([A-Z]|[a-z])[a-z]*(-)?[a-z]+$",
                            name_personne):
                # OM 2019.03.28 Message humiliant à l'attention de l'utilisateur.
                flash(f"Le nom est...incorrecte !!", "Danger")
                # On doit afficher à nouveau le formulaire "personnes_add.html" à cause des erreurs de "claviotage"
                return render_template("personnes/personnes_add.html")

            if not re.match("^([A-Z]|[a-z])[a-z]*(-)?[a-z]+$",
                            firstname_personne):
                # OM 2019.03.28 Message humiliant à l'attention de l'utilisateur.
                flash(f"Le prenom est...incorrecte !!", "Danger")
                # On doit afficher à nouveau le formulaire "personnes_add.html" à cause des erreurs de "claviotage"
                return render_template("personnes/personnes_add.html")

            if not re.match("^([12]\d{3}-(0[1-9]|1[0-2])-(0[1-9]|[12]\d|3[01]))$",
                            datenaissance_personne):
                # OM 2019.03.28 Message humiliant à l'attention de l'utilisateur.
                flash(f"La date de naissance est...incorrecte !!", "Danger")
                # On doit afficher à nouveau le formulaire "personnes_add.html" à cause des erreurs de "claviotage"
                return render_template("personnes/personnes_add.html")

            else:

                # Constitution d'un dictionnaire et insertion dans la BD
                valeurs_insertion_dictionnaire = {"value_NomPers": name_personne,
                                                  "value_PrenomPers": firstname_personne,
                                                  "value_DateNaissPers": datenaissance_personne}
                obj_actions_personnes.add_personne_data(valeurs_insertion_dictionnaire)

                # OM 2019.03.25 Les 2 lignes ci-après permettent de donner un sentiment rassurant aux utilisateurs.
                flash(f"Données insérées !!", "Sucess")
                print(f"Données insérées !!")
                # On va interpréter la "route" 'personnes_afficher', car l'utilisateur
                # doit voir le nouveau personne qu'il vient d'insérer.
                return redirect(url_for('personnes_afficher', order_by = 'DESC', id_personne_sel=0))

        # OM 2020.04.16 ATTENTION à l'ordre des excepts très important de respecter l'ordre.
        except pymysql.err.IntegrityError as erreur:
            # OM 2020.04.09 On dérive "pymysql.err.IntegrityError" dans "MaBdErreurDoublon" fichier "erreurs.py"
            # Ainsi on peut avoir un message d'erreur personnalisé.
            raise MaBdErreurDoublon(
                f"RGG pei {msg_erreurs['ErreurDoublonValue']['message']} et son status {msg_erreurs['ErreurDoublonValue']['status']}")

        # OM 2020.04.16 ATTENTION à l'ordre des excepts très important de respecter l'ordre.
        except (pymysql.err.OperationalError,
                pymysql.ProgrammingError,
                pymysql.InternalError,
                TypeError) as erreur:
            flash(f"Autre erreur {erreur}")
            raise MonErreur(f"Autre erreur")

        # OM 2020.04.16 ATTENTION à l'ordre des excepts très important de respecter l'ordre.
        except Exception as erreur:
            # OM 2020.04.09 On dérive "Exception" dans "MaBdErreurConnexion" fichier "erreurs.py"
            # Ainsi on peut avoir un message d'erreur personnalisé.
            raise MaBdErreurConnexion(
                f"RGG Exception {msg_erreurs['ErreurConnexionBD']['message']} et son status {msg_erreurs['ErreurConnexionBD']['status']}")
    # OM 2020.04.07 Envoie la page "HTML" au serveur.
    return render_template("personnes/personnes_add.html")


# ---------------------------------------------------------------------------------------------------
# OM 2020.04.07 Définition d'une "route" /personnes_edit , cela va permettre de programmer quelles actions sont réalisées avant de l'envoyer
# au navigateur par la méthode "render_template".
# On change la valeur d'un personne de personnes par la commande MySql "UPDATE"
# ---------------------------------------------------------------------------------------------------
@obj_mon_application.route('/personnes_edit', methods=['POST', 'GET'])
def personnes_edit():
    # OM 2020.04.07 Les données sont affichées dans un formulaire, l'affichage de la sélection
    # d'une seule ligne choisie par le bouton "edit" dans le formulaire "personnes_afficher.html"
    if request.method == 'GET':
        try:
            # Récupérer la valeur de "id_personne" du formulaire html "personnes_afficher.html"
            # l'utilisateur clique sur le lien "edit" et on récupére la valeur de "id_personne"
            # grâce à la variable "id_personne_edit_html"
            # <a href="{{ url_for('personnes_edit', id_personne_edit_html=row.id_personne) }}">Edit</a>
            id_personne_edit = request.values['id_personne_edit_html']

            # Pour afficher dans la console la valeur de "id_personne_edit", une façon simple de se rassurer,
            # sans utiliser le DEBUGGER
            print(id_personne_edit)

            # Constitution d'un dictionnaire et insertion dans la BD
            valeur_select_dictionnaire = {"value_id_personne": id_personne_edit}

            # OM 2020.04.09 Objet contenant toutes les méthodes pour gérer (CRUD) les données.
            obj_actions_personnes = GestionPersonnes()

            # OM 2019.04.02 La commande MySql est envoyée à la BD
            data_id_personne = obj_actions_personnes.edit_personne_data(valeur_select_dictionnaire)
            print("dataIdpersonne ", data_id_personne, "type ", type(data_id_personne))
            # Message ci-après permettent de donner un sentiment rassurant aux utilisateurs.
            flash(f"Editer le personne d'un film !!!")

        except (Exception,
                pymysql.err.OperationalError,
                pymysql.ProgrammingError,
                pymysql.InternalError,
                pymysql.IntegrityError,
                TypeError) as erreur:

            # On indique un problème, mais on ne dit rien en ce qui concerne la résolution.
            print("Problème avec la BD ! : %s", erreur)
            # OM 2020.04.09 On dérive "Exception" dans "MaBdErreurConnexion" fichier "erreurs.py"
            # Ainsi on peut avoir un message d'erreur personnalisé.
            raise MaBdErreurConnexion(f"RGG Exception {msg_erreurs['ErreurConnexionBD']['message']}"
                                      f"et son status {msg_erreurs['ErreurConnexionBD']['status']}")

    return render_template("personnes/personnes_edit.html", data=data_id_personne)


# ---------------------------------------------------------------------------------------------------
# OM 2020.04.07 Définition d'une "route" /personnes_update , cela va permettre de programmer quelles actions sont réalisées avant de l'envoyer
# au navigateur par la méthode "render_template".
# On change la valeur d'un personne de personnes par la commande MySql "UPDATE"
# ---------------------------------------------------------------------------------------------------
@obj_mon_application.route('/personnes_update', methods=['POST', 'GET'])
def personnes_update():
    # DEBUG bon marché : Pour afficher les méthodes et autres de la classe "flask.request"
    print(dir(request))
    # OM 2020.04.07 Les données sont affichées dans un formulaire, l'affichage de la sélection
    # d'une seule ligne choisie par le bouton "edit" dans le formulaire "personnes_afficher.html"
    # Une fois que l'utilisateur à modifié la valeur du personne alors il va appuyer sur le bouton "UPDATE"
    # donc en "POST"
    if request.method == 'POST':
        try:
            # DEBUG bon marché : Pour afficher les valeurs contenues dans le formulaire
            print("request.values ", request.values)

            # Récupérer la valeur de "id_personne" du formulaire html "personnes_edit.html"
            # l'utilisateur clique sur le lien "edit" et on récupére la valeur de "id_personne"
            # grâce à la variable "id_personne_edit_html"
            # <a href="{{ url_for('personnes_edit', id_personne_edit_html=row.id_personne) }}">Edit</a>
            id_personne_edit = request.values['id_personne_edit_html']

            print(id_personne_edit)

            # Récupère le contenu du champ "NomPers" dans le formulaire HTML "personnesEdit.html"
            name_personne = request.values['name_edit_NomPers_html']
            firstname_personne = request.values['name_edit_PrenomPers_html']
            datenaissance_personne = request.values['name_edit_DateNaissPers_html']

            valeur_edit_list = [{'id_personne': id_personne_edit, 'NomPers': name_personne, 'PrenomPers': firstname_personne, 'DateNaissPers': datenaissance_personne}]

            # Regex n'acceptant que les noms valides
            if not re.match("^([A-Z]|[a-z])[a-z]*(-)?[a-z]+$", name_personne):
                # En cas d'erreur, conserve la saisie fausse, afin que l'utilisateur constate sa misérable faute
                flash(f"Le nom n'est pas valide !! Pas de chiffres, de caractères spéciaux, d'espace"
                      f"Danger")

                # On doit afficher à nouveau le formulaire "personnes_edit.html" à cause des erreurs de "claviotage"
                valeur_edit_list = [{'id_personne': id_personne_edit, 'NomPers': name_personne, 'PrenomPers': firstname_personne, 'DateNaissPers': datenaissance_personne}]

                # DEBUG bon marché :
                # Pour afficher le contenu et le type de valeurs passées au formulaire "personnes_edit.html"
                print(valeur_edit_list, "type ..", type(valeur_edit_list))
                return render_template('personnes/personnes_edit.html', data=valeur_edit_list)

            # Regex n'acceptant que les noms valides
            if not re.match("^([A-Z]|[a-z])[a-z]*(-)?[a-z]+$", firstname_personne):
                # En cas d'erreur, conserve la saisie fausse, afin que l'utilisateur constate sa misérable faute
                flash(f"Le prenom n'est pas valide !! Pas de chiffres, de caractères spéciaux, d'espace"
                      f"Danger")

                # On doit afficher à nouveau le formulaire "personnes_edit.html" à cause des erreurs de "claviotage"
                valeur_edit_list = [{'id_personne': id_personne_edit, 'NomPers': name_personne, 'PrenomPers': firstname_personne, 'DateNaissPers': datenaissance_personne}]

                # DEBUG bon marché :
                # Pour afficher le contenu et le type de valeurs passées au formulaire "personnes_edit.html"
                print(valeur_edit_list, "type ..", type(valeur_edit_list))
                return render_template('personnes/personnes_edit.html', data=valeur_edit_list)

            # Regex n'acceptant que les dates avec une structure valide, format yyyy-mm-dd
            if not re.match("^([12]\d{3}-(0[1-9]|1[0-2])-(0[1-9]|[12]\d|3[01]))$", datenaissance_personne):
                # En cas d'erreur, conserve la saisie fausse, afin que l'utilisateur constate sa misérable faute
                flash(f"La date est incorrecte !!", "Danger")

                # On doit afficher à nouveau le formulaire "personnes_edit.html" à cause des erreurs de "claviotage"
                valeur_edit_list = [{'id_personne': id_personne_edit, 'NomPers': name_personne, 'PrenomPers': firstname_personne, 'DateNaissPers': datenaissance_personne}]

                # DEBUG bon marché :
                # Pour afficher le contenu et le type de valeurs passées au formulaire "personnes_edit.html"
                print(valeur_edit_list, "type ..", type(valeur_edit_list))
                return render_template('personnes/personnes_edit.html', data=valeur_edit_list)

            else:
                # Constitution d'un dictionnaire et insertion dans la BD
                valeur_update_dictionnaire = {"value_id_personne": id_personne_edit,
                                              "value_NomPers": name_personne,
                                              "value_PrenomPers": firstname_personne,
                                              "value_DateNaissPers": datenaissance_personne}

                # OM 2020.04.09 Objet contenant toutes les méthodes pour gérer (CRUD) les données.
                obj_actions_personnes = GestionPersonnes()

                # La commande MySql est envoyée à la BD
                data_id_personne = obj_actions_personnes.update_personne_data(valeur_update_dictionnaire)
                # DEBUG bon marché :
                print("dataIdpersonne ", data_id_personne, "type ", type(data_id_personne))
                # Message ci-après permettent de donner un sentiment rassurant aux utilisateurs.
                flash(f"Editer le personne d'un film !!!")
                # On affiche les personnes
                return redirect(url_for('personnes_afficher', order_by="ASC", id_personne_sel=id_personne_edit))

        except (Exception,
                pymysql.err.OperationalError,
                pymysql.ProgrammingError,
                pymysql.InternalError,
                pymysql.IntegrityError,
                TypeError) as erreur:

            print(erreur.args)
            flash(f"problème personnes update{erreur.args[0]}")
            # En cas de problème, mais surtout en cas de non respect
            # des régles "REGEX" dans le champ "name_edit_intitule_personne_html" alors on renvoie le formulaire "EDIT"
    return render_template('personnes/personnes_edit.html', data=valeur_edit_list)


# ---------------------------------------------------------------------------------------------------
# OM 2020.04.07 Définition d'une "route" /personnes_select_delete , cela va permettre de programmer quelles actions sont réalisées avant de l'envoyer
# au navigateur par la méthode "render_template".
# On change la valeur d'un personne de personnes par la commande MySql "UPDATE"
# ---------------------------------------------------------------------------------------------------
@obj_mon_application.route('/personnes_select_delete', methods=['POST', 'GET'])
def personnes_select_delete():
    if request.method == 'GET':
        try:

            # OM 2020.04.09 Objet contenant toutes les méthodes pour gérer (CRUD) les données.
            obj_actions_personnes = GestionPersonnes()
            # OM 2019.04.04 Récupérer la valeur de "idpersonneDeleteHTML" du formulaire html "personnesDelete.html"
            id_personne_delete = request.args.get('id_personne_delete_html')
            print("test")
            # Constitution d'un dictionnaire et insertion dans la BD
            valeur_delete_dictionnaire = {"value_id_personne": id_personne_delete}

            # OM 2019.04.02 La commande MySql est envoyée à la BD
            data_id_personne = obj_actions_personnes.delete_select_personne_data(valeur_delete_dictionnaire)
            flash(f"EFFACER et c'est terminé pour cette \"POV\" valeur !!!")

        except (Exception,
                pymysql.err.OperationalError,
                pymysql.ProgrammingError,
                pymysql.InternalError,
                pymysql.IntegrityError,
                TypeError) as erreur:
            # Communiquer qu'une erreur est survenue.
            # DEBUG bon marché : Pour afficher un message dans la console.
            print(f"Erreur personnes_delete {erreur.args[0], erreur.args[1]}")
            # C'est une erreur à signaler à l'utilisateur de cette application WEB.
            flash(f"Erreur personnes_delete {erreur.args[0], erreur.args[1]}")

    # Envoie la page "HTML" au serveur.
    return render_template('personnes/personnes_delete.html', data=data_id_personne)


# ---------------------------------------------------------------------------------------------------
# OM 2019.04.02 Définition d'une "route" /personnesUpdate , cela va permettre de programmer quelles actions sont réalisées avant de l'envoyer
# au navigateur par la méthode "render_template".
# Permettre à l'utilisateur de modifier un personne, et de filtrer son entrée grâce à des expressions régulières REGEXP
# ---------------------------------------------------------------------------------------------------
@obj_mon_application.route('/personnes_delete', methods=['POST', 'GET'])
def personnes_delete():
    # OM 2019.04.02 Pour savoir si les données d'un formulaire sont un affichage ou un envoi de donnée par des champs utilisateurs.
    if request.method == 'POST':
        try:
            # OM 2020.04.09 Objet contenant toutes les méthodes pour gérer (CRUD) les données.
            obj_actions_personnes = GestionPersonnes()
            # OM 2019.04.02 Récupérer la valeur de "id_personne" du formulaire html "personnesAfficher.html"
            id_personne_delete = request.form['id_personne_delete_html']
            # Constitution d'un dictionnaire et insertion dans la BD
            valeur_delete_dictionnaire = {"value_id_personne": id_personne_delete}

            data_personnes = obj_actions_personnes.delete_personne_data(valeur_delete_dictionnaire)
            # OM 2019.04.02 On va afficher la liste des personnes des personnes
            # OM 2019.04.02 Envoie la page "HTML" au serveur. On passe un message d'information dans "message_html"

            # On affiche les personnes
            return redirect(url_for('personnes_afficher',order_by="ASC",id_personne_sel=0))



        except (pymysql.err.OperationalError, pymysql.ProgrammingError, pymysql.InternalError, pymysql.IntegrityError,
                TypeError) as erreur:
            # OM 2020.04.09 Traiter spécifiquement l'erreur MySql 1451
            # Cette erreur 1451, signifie qu'on veut effacer un "personne" de personnes qui est associé dans "t_pers_a_personnes".
            if erreur.args[0] == 1451:
                # C'est une erreur à signaler à l'utilisateur de cette application WEB.
                flash('IMPOSSIBLE d\'effacer !!! Cette valeur est associée à des personness !')
                # DEBUG bon marché : Pour afficher un message dans la console.
                print(
                    f"IMPOSSIBLE d'effacer !! Ce personne est associé à des personness dans la t_pers_a_personnes !!! : {erreur}")
                # Afficher la liste des personnes des personnes
                return redirect(url_for('personnes_afficher', order_by="ASC", id_personne_sel=0))
            else:
                # Communiquer qu'une autre erreur que la 1062 est survenue.
                # DEBUG bon marché : Pour afficher un message dans la console.
                print(f"Erreur personnes_delete {erreur.args[0], erreur.args[1]}")
                # C'est une erreur à signaler à l'utilisateur de cette application WEB.
                flash(f"Erreur personnes_delete {erreur.args[0], erreur.args[1]}")

            # OM 2019.04.02 Envoie la page "HTML" au serveur.
    return render_template('personnes/personnes_afficher.html', data=data_personnes)
